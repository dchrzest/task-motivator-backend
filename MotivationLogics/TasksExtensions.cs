﻿using System;
using System.Collections.Generic;

namespace MotivationLogics
{
    public static class TasksExtensions
    {
        public static List<DateTime> TaskTimesToDo()
        {
            return null;
        }
        public static List<DateTime> TasksEndedNotChecked()
        {

        }

        private List<TaskViewmodel> EventTimesInWeek(SimpleTask x)
        {
            List<TaskViewmodel> times = new List<TaskViewmodel>();
            var currentTime = DateTime.Now;
            x.RepeatingPeriods.ToList().ForEach(y =>
            {
                switch (y.RepeatPeriod)
                {
                    case RepeatPeriod.EveryDay:
                        if (x.LastTimeDone.Date != currentTime.Date)
                        {
                            times.Add(new TaskViewmodel
                            {
                                Name = x.Name,
                                Description = x.Description,
                                IsDone = false,
                                NearestDeadline = currentTime.Date,
                                Points = x.Points,
                                IsRepeating = x.IsRepeating
                            });
                        }
                        break;
                    case RepeatPeriod.EveryWeek:
                        var nextEvent = currentTime.Next(y.StartTime.DayOfWeek);
                        if (nextEvent.)
                    case RepeatPeriod.EveryMonth:
                        lastDeadline = DateTime.Now.Last(y.StartTime.Day);
                        if (x.LastTimeDone < lastDeadline)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    default:
                        break;
                }
            });
        }
    }
}
