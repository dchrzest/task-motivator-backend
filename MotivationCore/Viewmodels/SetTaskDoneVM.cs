﻿using MotivationCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotivationCore.Viewmodels
{
    public class SetTaskDoneVM
    {
        public long SimpleTaskId{ get; set; }
        public long TaskDateId { get; set; }
        public DateTime TimeOfTaskDeadline { get; set; }
    }
}
