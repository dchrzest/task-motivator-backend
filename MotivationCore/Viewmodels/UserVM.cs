﻿using MoticationCore.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotivationCore.Viewmodels
{
    public class UserVM
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public AuthenticationType AuthenticationType { get; set; }
    }
}
