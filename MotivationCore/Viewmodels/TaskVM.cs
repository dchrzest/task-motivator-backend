﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotivationCore.Viewmodels
{
    public class TaskVM
    {
        public long SimpleTaskId { get; set; }
        public long TaskDateId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Points { get; set; }
        public bool HasDeadline{ get; set; }
        public DateTime Deadline{ get; set; }
    }
}
