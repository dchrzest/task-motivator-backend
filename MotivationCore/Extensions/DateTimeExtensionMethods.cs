﻿using System;

namespace MotivationCore.Extensions
{
    public static class DateTimeExtensionMethods
    {
        public static DateTime ChangeTime(this DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                hours,
                minutes,
                seconds,
                milliseconds,
                dateTime.Kind);
        }

        public static DateTime ChangeTime(this DateTime dateTime, TimeSpan time)
        {
            return new DateTime(
                dateTime.Year,
                dateTime.Month,
                dateTime.Day,
                time.Hours,
                time.Minutes,
                time.Seconds,
                time.Milliseconds);
        }

        public static DateTime NextEveryday(this DateTime baseTime)
        {
            if (baseTime.TimeOfDay > DateTime.Now.TimeOfDay)
                return DateTime.Now.ChangeTime(baseTime.TimeOfDay);
            else
                return DateTime.Now.AddDays(1).ChangeTime(baseTime.TimeOfDay);
        }

        public static DateTime NextEveryday(this DateTime baseTime, DateTime from)
        {
            if (baseTime.AddSeconds(-1).TimeOfDay > from.TimeOfDay)
                return from.ChangeTime(baseTime.TimeOfDay);
            else
                return from.AddDays(1).ChangeTime(baseTime.TimeOfDay);
        }

        public static DateTime NextEveryWeek(this DateTime baseTime)
        {
            DateTime now = DateTime.Now;

            if (now.DayOfWeek > baseTime.DayOfWeek)
                now = now.AddDays(7);
            else if (now.DayOfWeek == baseTime.DayOfWeek)
            {
                if (now.TimeOfDay >= baseTime.TimeOfDay)
                {
                    now.AddDays(7);
                }
            }

            return now.AddDays(baseTime.DayOfWeek - now.DayOfWeek).ChangeTime(baseTime.TimeOfDay);
        }

        public static DateTime NextEveryWeek(this DateTime baseTime, DateTime from)
        {
            if (from.DayOfWeek >= baseTime.DayOfWeek)
                from = from.AddDays(7);
            else if (from.DayOfWeek == baseTime.DayOfWeek)
            {
                if (from.TimeOfDay >= baseTime.TimeOfDay)
                {
                    from.AddDays(7);
                }
            }

            return from.AddDays(baseTime.DayOfWeek - from.DayOfWeek).ChangeTime(baseTime.TimeOfDay);
        }

        public static DateTime NextEveryMonth(this DateTime baseTime)
        {
            DateTime now = DateTime.Now;
            if (now.Day > baseTime.Day)
                now = now.AddMonths(1);
            else if (now.Day == baseTime.Day)
            {
                if (now.TimeOfDay >= baseTime.TimeOfDay)
                {
                    now.AddMonths(1);
                }
            }

            return now.AddDays(baseTime.Day - now.Day).ChangeTime(baseTime.TimeOfDay);
        }

        public static DateTime NextEveryMonth(this DateTime baseTime, DateTime from)
        {
            if (from.Day > baseTime.Day)
                from = from.AddMonths(1);
            else if (from.Day == baseTime.Day)
            {
                if (from.TimeOfDay >= baseTime.TimeOfDay)
                {
                    from.AddMonths(1);
                }
            }

            return from.AddDays(baseTime.Day - from.Day).ChangeTime(baseTime.TimeOfDay);
        }
    }
}
