﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MoticationCore.Shared
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AuthenticationType { User, Editor, Admin };
}
