﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MotivationCore.Migrations
{
    public partial class endtimeselected : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EndTimeSelected",
                table: "SimpleTasks",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndTimeSelected",
                table: "SimpleTasks");
        }
    }
}
