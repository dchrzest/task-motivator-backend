﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MotivationCore.Migrations
{
    public partial class isskipped : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSkipped",
                table: "DoneTasks",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSkipped",
                table: "DoneTasks");
        }
    }
}
