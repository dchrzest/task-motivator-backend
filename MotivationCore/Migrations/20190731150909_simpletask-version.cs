﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MotivationCore.Migrations
{
    public partial class simpletaskversion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "EndTime",
                table: "SimpleTasks",
                nullable: false,
                defaultValue: new DateTime(2039, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsArchived",
                table: "SimpleTasks",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Version",
                table: "SimpleTasks",
                nullable: false,
                defaultValue: 1);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndTime",
                table: "SimpleTasks");

            migrationBuilder.DropColumn(
                name: "IsArchived",
                table: "SimpleTasks");

            migrationBuilder.DropColumn(
                name: "Version",
                table: "SimpleTasks");
        }
    }
}
