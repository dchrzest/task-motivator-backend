﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MotivationCore.Extensions;
using MotivationCore.Models;
using MotivationCore.Viewmodels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MotivationCore.Core
{
    public enum StatusOfTask
    {
        Done,
        ToCheck,
        ToShow
    }

    public struct TaskInfo
    {
        public List<StatusOfTask> taskStatus;
        public DateTime deadline;
    }

    public class TaskCheckedInfo
    {
        public SimpleTask task { get; set; }
        public TaskDate date { get; set; }

    }

    public static class TaskHelper
    {
        public static List<TaskVM> GetAllDoneTasks(IQueryable<DoneTask> doneTasks)
        {
            List<TaskVM> tasks = new List<TaskVM>();
            doneTasks.ToList().ForEach(x =>
            {
                tasks.Add(SimpleTaskToViewmodel(x.Task, x.DoneTime));
            });
            return tasks;
        }

        public static List<TaskVM> GetDoneTasks(List<DoneTask> doneTasks, DateTime from, DateTime to)
        {
            List<TaskVM> tasks = new List<TaskVM>();
            doneTasks.ToList().ForEach(x =>
            {
                if (x.DoneTime > from)
                    tasks.Add(SimpleTaskToViewmodel(x.Task, x.DoneTime));
            });
            return tasks;
        }
        public static DateTime GetNearestDeadline(TaskDate date, DateTime taskEndTime)
        {
            switch (date.RepeatPeriod)
            {
                case RepeatPeriod.EveryDay:
                    return date.StartTime.NextEveryday();
                case RepeatPeriod.EveryWeek:
                    return date.StartTime.NextEveryWeek();
                case RepeatPeriod.EveryMonth:
                    return date.StartTime.NextEveryMonth();
                case RepeatPeriod.NoRepeat:
                    return date.StartTime;
                default:
                    throw new Exception("wrong repeat period");
            }
        }

        public static List<TaskVM> GetCurrentTasks(List<SimpleTask> userTasks, List<DoneTask> userDoneTasks)
        {
            List<TaskVM> result = new List<TaskVM>();
            userTasks.ForEach(task =>
            {
                if (task.IsRepeating == false && task.IsDone == false)
                {
                    result.Add(SimpleTaskToViewmodel(task));
                }
                else
                {
                    task.RepeatingPeriods?.ToList().ForEach(dateTask =>
                    {
                        var nextTaskDate = GetNearestDeadline(dateTask, task.EndTime);
                        if (!(task.EndTimeSelected && IsExpired(nextTaskDate, task.EndTime)))
                        {
                            var isAlreadyDone = userDoneTasks.FirstOrDefault(doneTask => doneTask.DoneTime == nextTaskDate && doneTask.TaskDate == dateTask);
                            if (isAlreadyDone == null)
                                result.Add(SimpleTaskToViewmodel(task, dateTask));
                        }
                    });
                }
            });
            return result;
        }

        public static List<TaskVM> GetTasksToCheck(List<SimpleTask> userTasks, List<DoneTask> userDoneTasks)
        {
            List<TaskVM> result = new List<TaskVM>();
            userTasks.ToList().ForEach(task =>
            {
                task.RepeatingPeriods.ToList().ForEach(period =>
                {
                    var taskTimesInPeriod = AllTimesTaskInTime(period, DateTime.Now.AddDays(-7), DateTime.Now, task.EndTime, task.EndTimeSelected);
                    IEnumerable<DateTime> filteredTasks = UncheckedTasks(userDoneTasks, period, taskTimesInPeriod);
                    filteredTasks.ToList().ForEach(dateToCheck =>
                    {
                        result.Add(SimpleTaskToViewmodel(task, dateToCheck));
                    });
                });
            });
            return result;
        }

        private static TaskVM SimpleTaskToViewmodel(SimpleTask task, TaskDate taskData)
        {
            return new TaskVM
            {
                Name = task.Name,
                Description = task.Description,
                Points = task.Points,
                SimpleTaskId = task.Id,
                TaskDateId = taskData.Id,
                HasDeadline = true,
                Deadline = GetNearestDeadline(taskData, task.EndTime)
            };
        }

        private static TaskVM SimpleTaskToViewmodel(SimpleTask task, DateTime deadline)
        {
            return new TaskVM
            {
                Name = task.Name,
                Description = task.Description,
                Points = task.Points,
                SimpleTaskId = task.Id,
                HasDeadline = true,
                Deadline = deadline
            };
        }

        private static TaskVM SimpleTaskToViewmodel(SimpleTask task)
        {
            return new TaskVM
            {
                Name = task.Name,
                Description = task.Description,
                Points = task.Points,
                SimpleTaskId = task.Id,
                HasDeadline = task.EndTimeSelected,
                Deadline = task.EndTime
            };
        }

        private static bool IsExpired(DateTime deadline, DateTime endTime)
        {
            return deadline > endTime;
        }

        private static DateTime GetNextTime(TaskDate date, DateTime time)
        {
            switch (date.RepeatPeriod)
            {
                case RepeatPeriod.EveryDay:
                    return date.StartTime.NextEveryday(time);
                case RepeatPeriod.EveryWeek:
                    return date.StartTime.NextEveryWeek(time);
                case RepeatPeriod.EveryMonth:
                    return date.StartTime.NextEveryMonth(time);
                default:
                    throw new NotSupportedException();
            }
        }

        private static List<DateTime> AllTimesTaskInTime(TaskDate taskDate, DateTime from, DateTime to, DateTime endTime, bool endTimeSelected)
        {
            List<DateTime> times = new List<DateTime>();
            DateTime currentWorkingOn = from;
            if (taskDate.StartTime > currentWorkingOn)
                currentWorkingOn = taskDate.StartTime;

            while (currentWorkingOn < to)
            {
                currentWorkingOn = GetNextTime(taskDate, currentWorkingOn);
                if (currentWorkingOn < endTime || !endTimeSelected)
                {
                    times.Add(currentWorkingOn);
                }
            }
            return times;
        }

        private static IEnumerable<DateTime> UncheckedTasks(List<DoneTask> userDoneTasks, TaskDate period, List<DateTime> taskTimesInPeriod)
        {
            return taskTimesInPeriod.Where(time => userDoneTasks.FirstOrDefault(doneTask => doneTask.DoneTime == time && doneTask.TaskDate == period) != null);
        }
    }
}
