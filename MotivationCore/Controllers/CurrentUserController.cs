﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MotivationCore.Models;
using MotivationCore.Viewmodels;
using MotivationCore.Web.Services;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MotivationCore.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class CurrentUserController : Controller
    {
        private readonly ApplicationUser applicationUser;
        private readonly MotivationCoreDbContext dbContext;

        public CurrentUserController(
            UserResolverService service,
            MotivationCoreDbContext dbContext)
        {
            this.dbContext = dbContext;
            applicationUser = service.GetUser();
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetUserData()
        {
            return Ok(new UserVM
            {
                Name = applicationUser.Name,
                Surname = applicationUser.Surname,
                Email = applicationUser.Email,
                AuthenticationType = applicationUser.AuthenticationType
            });
        }
    }
}
