﻿using ClothesManager.Web.Dtos;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MotivationCore.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace SBDBackend.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IConfiguration configuration;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration
            )
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
        }

        [HttpPost]
        public async Task<object> Login([FromBody] LoginDto model)
        {
            var appUser = userManager.Users.SingleOrDefault(r => r.Email == model.Email);
            if (appUser == null)
                return BadRequest("Wrong login or password");

            var result = await signInManager.PasswordSignInAsync(appUser.UserName, model.Password, false, false);

            if (result.Succeeded)
            {
                return GenerateJwtToken(model.Email, appUser);
            }
            else
            {
                return BadRequest("Wrong login or password");
            }

            throw new ApplicationException("INVALID_LOGIN_ATTEMPT");
        }


        [HttpPost]
        public async Task<object> Register([FromBody] RegisterDto model)
        {

            var user = new ApplicationUser
            {
                Name = model.Name, 
                Surname = model.Surname,
                UserName = model.Email,
                Email = model.Email,
                SumOfPoints = 0,
                AuthenticationType = MoticationCore.Shared.AuthenticationType.User
            };
            try
            {
                var result = await userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(user, false);
                    return GenerateJwtToken(model.Email, user);
                }
                else
                {
                    return BadRequest(result.ToString());
                }
            }
            catch (Exception x)
            {
                return BadRequest(x.ToString());
            }

        }

        private string GenerateJwtToken(string email, ApplicationUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                configuration["JwtIssuer"],
                configuration["JwtAudience"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}