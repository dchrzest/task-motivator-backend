﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MotivationCore.Extensions;
using MotivationCore.Models;
using MotivationCore.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MotivationCore.Viewmodels;
using MotivationCore.Core;

namespace MotivationCore.Controllers
{

    [ApiController]
    [Route("api/[controller]/[action]")]
    public class SimpleTasksController : Controller
    {
        private readonly MotivationCoreDbContext dbContext;
        private readonly ApplicationUser applicationUser;
        public SimpleTasksController(
            MotivationCoreDbContext context,
            UserResolverService userResolverService
            )
        {
            this.dbContext = context;
            applicationUser = userResolverService.GetUser();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] SimpleTask task)
        {
            if (task.RepeatingPeriods == null)
            {
                task.RepeatingPeriods = new List<TaskDate>();
            }
            //task.RepeatingPeriods.ToList().ForEach(x => dbContext.TaskDates.Add(x));
            //dbContext.SaveChanges();

            //var listOfRepeatingPeriods = dbContext.TaskDates.ToList();
            //dbContext.TaskDates.AddRange(task.RepeatingPeriods);
            dbContext.SimpleTasks.Add(new SimpleTask
            {
                CreationTime = DateTime.Now,
                Name = task.Name,
                TaskType = TaskType.Deadline,
                Description = task.Description,
                User = applicationUser,
                IsDone = false,
                IsArchived = false,
                RepeatingPeriods = task.RepeatingPeriods,
                IsRepeating = task.IsRepeating,
                Points = task.Points
            });
            try
            {
                await dbContext.SaveChangesAsync();

            }
            catch(Exception x)
            {
                return BadRequest(x.ToString());
            }
            return Ok();
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var habits = await dbContext.SimpleTasks.Where(x => x.User.Id == applicationUser.Id).ToArrayAsync();
                return Ok(habits);
            }
            catch (Exception x)
            {
                return BadRequest(x.ToString());
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult SkipTask([FromBody]SetTaskDoneVM model)
        {
            var task = dbContext.SimpleTasks.FirstOrDefault(x => model.SimpleTaskId == x.Id);
            if (task == null)
                return BadRequest("Simple task not found in database");

            var taskDate = task.RepeatingPeriods.FirstOrDefault(x => x.Id == model.TaskDateId);

            if (taskDate == null)
                return BadRequest("Task Date not found in database");

            var taskSkipped = new DoneTask
            {
                User = applicationUser,
                TaskDate = taskDate,
                Task = task,
                IsSkipped = true,
                DoneTime = model.TimeOfTaskDeadline
            };
            dbContext.DoneTasks.Add(taskSkipped);
            dbContext.SaveChanges();
            return Ok();
        }

        [Authorize]
        [HttpPost]
        public IActionResult SetTaskDone([FromBody]SetTaskDoneVM model)
        {

            var task = dbContext.SimpleTasks.FirstOrDefault(x => model.SimpleTaskId == x.Id);
            if (task == null)
                return BadRequest("Simple task not found in database");

            var taskDate = task.RepeatingPeriods.FirstOrDefault(x => x.Id == model.TaskDateId);

            if (taskDate == null)
                return BadRequest("Task Date not found in database");

            var taskDone = new DoneTask
            {
                User = applicationUser,
                TaskDate = taskDate,
                Task = task,
                DoneTime = model.TimeOfTaskDeadline
            };
            dbContext.DoneTasks.Add(taskDone);
            dbContext.SaveChanges();
            return Ok();
        }

        //[Authorize]
        //[HttpGet]
        //public async Task<IActionResult> GetTasksToDo()
        //{
        //    var userTasks = dbContext.SimpleTasks.Where(x => x.User == applicationUser);
        //    await userTasks.ForEachAsync(x =>
        //    {
        //        x.RepeatingPeriods.ToList().ForEach(y =>
        //        {

        //        });
        //    }).ContinueWith(action =>
        //    {
        //        return Ok();
        //    });
        //    return BadRequest();
        //}

        [Authorize]
        [HttpGet]
        public IActionResult GetTasksToCheck()
        {
            var userTasks = dbContext.SimpleTasks.Include(y => y.RepeatingPeriods).Where(x => x.User == applicationUser).ToList();
            var userDoneTasks = dbContext.DoneTasks.Where(x => x.User == applicationUser).ToList();
            List<TaskVM> tasksToCheck = TaskHelper.GetTasksToCheck(userTasks, userDoneTasks);
            return Ok(tasksToCheck);
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetCurrentTasks()
        {
            var userTasks = dbContext.SimpleTasks.Where(x => x.User == applicationUser).ToList();
            var userDoneTasks = dbContext.DoneTasks.Where(x => x.User == applicationUser).ToList();
            List<TaskVM> tasksToDo = TaskHelper.GetCurrentTasks(userTasks, userDoneTasks);
            return Ok(tasksToDo);
        }

        public IActionResult GetFullHistoryOfDoneTasks()
        {
            var userDoneTasks = dbContext.DoneTasks.Where(x => x.User == applicationUser);
            List<TaskVM> doneTasks = TaskHelper.GetAllDoneTasks(userDoneTasks);
            return Ok(doneTasks);
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetDoneTasks()
        {
            var userDoneTasks = dbContext.DoneTasks.Where(x => x.User == applicationUser).ToList();
            List<TaskVM> doneTasksBetweenTimes = TaskHelper.GetDoneTasks(userDoneTasks, DateTime.Now.AddDays(-7), DateTime.Now);
            return Ok(doneTasksBetweenTimes);

        }

        [Authorize]
        [HttpGet]
        public IActionResult GetTask([FromQuery] long Id)
        {
            var task = dbContext.SimpleTasks.FirstOrDefault(x => x.Id == Id);
            if (task == null)
                return BadRequest("Task with that Id not found");
            if (task.User.Id != applicationUser.Id)
                return Unauthorized();

            return Ok(task);
        }

        [Authorize]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery] long Id)
        {
            var task = dbContext.SimpleTasks.FirstOrDefault(x => x.Id == Id);
            if (task == null)
            {
                return NotFound();
            }

            if (task.User.Id != applicationUser.Id)
            {
                return Ok("This user is not able to delete this task");
            }

            dbContext.SimpleTasks.Remove(task);
            await dbContext.SaveChangesAsync();

            return Ok();
        }

        [Authorize]
        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] SimpleTask task)
        {
            var dbTask = dbContext.SimpleTasks.FirstOrDefault(x => x.Id == task.Id);
            if (dbTask == null)
            {
                return NotFound();
            }

            if (task.User.Id != applicationUser.Id)
            {
                return Ok("This user is not able to delete this task");
            }

            var newEditedTask = new SimpleTask(
                task.Name,
                task.Description,
                task.Points,
                task.User,
                task.IsRepeating,
                task.TaskType,
                task.EndTime,
                task.RepeatingPeriods,
                ++task.Version);
            dbContext.SimpleTasks.Add(newEditedTask);
            dbTask.IsArchived = true;
            await dbContext.SaveChangesAsync();
            return Ok();
        }
    }
}
