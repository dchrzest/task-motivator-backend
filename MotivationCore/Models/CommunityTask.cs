﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotivationCore.Models
{
    public class CommunityTask
    {
        public ApplicationUser Author { get; set; }
        public List<TaskRate> Ratings { get; set; }
        public string Description { get; set; }
        public DateTime CreationTime { get; set; }
        public double AverateRating
        {
            get
            {
                return Ratings.Average(x => x.Rating);
            }
        }
    }
}

