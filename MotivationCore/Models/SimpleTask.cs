﻿using System;
using System.Collections.Generic;

namespace MotivationCore.Models
{
    public enum TaskType
    {
        Deadline,
        Event
    }

    public class SimpleTask
    {
        public SimpleTask()
        {
            CreationTime = DateTime.Now;
        }

        public SimpleTask(
            string name,
            string description,
            int points,
            ApplicationUser user,
            bool isRepeating,
            TaskType taskType,
            DateTime endTime,
            ICollection<TaskDate> repeatingPeriods,
            int version = 1)
        {
            CreationTime = DateTime.Now;
            Name = name;
            Description = description;
            Points = points;
            IsRepeating = isRepeating;
            TaskType = taskType;
            User = user;
            RepeatingPeriods = repeatingPeriods;
            EndTime = endTime;
            Version = version;
        }

        public long Id { get; set; }
        public int Points { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDone { get; set; }
        public bool IsArchived { get; set; }
        public int Version { get; set; }
        public bool IsRepeating { get; set; }
        public TaskType TaskType { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool EndTimeSelected { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<TaskDate> RepeatingPeriods { get; set; }
    }
}
