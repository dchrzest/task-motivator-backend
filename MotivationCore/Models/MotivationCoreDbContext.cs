﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;
using System.Threading.Tasks;
namespace MotivationCore.Models
{
    public class MotivationCoreDbContext : IdentityDbContext<ApplicationUser>
    {
        public MotivationCoreDbContext(DbContextOptions<MotivationCoreDbContext> options) : base(options)
        {
       
        }
        public virtual DbSet<SimpleTask> SimpleTasks { get; set; }
        public virtual DbSet<TaskDate> TaskDates { get; set; }
        public virtual DbSet<DoneTask> DoneTasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }

}
