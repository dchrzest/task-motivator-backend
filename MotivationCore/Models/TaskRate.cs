﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotivationCore.Models
{
    public class TaskRate
    {
        public ApplicationUser User { get; set; }
        public int Rating { get; set; }
    }
}
