﻿using System;

namespace MotivationCore.Models
{
    public enum RepeatPeriod
    {
        EveryDay,
        EveryMonth,
        EveryWeek,
        NoRepeat
    }
    public class TaskDate
    {
        public long Id { get; set; }
        public DateTime StartTime { get; set; }
        public RepeatPeriod RepeatPeriod { get; set; }
    }
}
