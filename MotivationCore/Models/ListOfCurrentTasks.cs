﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotivationCore.Models
{
    public class ListOfMainTasks
    {
        public ICollection<Task> CurrentTasks { get; set; }
        public ICollection<Task> DoneTasks { get; set; }
    }
}
