﻿using Microsoft.AspNetCore.Identity;
using MoticationCore.Shared;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SBDBackend.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotivationCore.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public AuthenticationType AuthenticationType { get; set; }
        public int SumOfPoints { get; set; }

        public string Fullname
        {
            get
            {
                return Name + " " + Surname;
            }
        }
    }
}
