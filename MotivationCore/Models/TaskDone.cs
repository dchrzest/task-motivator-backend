﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MotivationCore.Models
{
    public class DoneTask
    {
        public long Id { get; set; }
        public bool IsSkipped { get; set; }
        public virtual ApplicationUser User{ get; set; }
        public virtual SimpleTask Task { get; set; }
        public virtual TaskDate TaskDate { get; set; }
        public DateTime DoneTime { get; set; }
    }
}
